Dedecus Font by Font Monger. Designed by Chris Vile (www.chrisvile.com)

Font usage: Freeware (Free for personal use only) Non commercial

For a commercial usage license visit fontmonger.com or:

http://www.fontmonger.com/commercial-fonts.html#!/Font-License-for-Dedecus/p/45718933/category=0

Remember the commercial license also provides numbers, punctuation, different font styles (Regular, Bold, Italic, Italic Bold, Thin, and Ultra Thin)

contact: info@fontmonger.com

*** social media ***
http://www.facebook.com/fontmonger
http://www.facebook.com/designsbychris
http://www.twitter.com/fontmonger
http://www.twitter.com/chris_vile

*** My font sites ***
http://www.chrisvile.com
http://www.fontmonger.com
http://www.type512.com
http://www.fontillery.com

